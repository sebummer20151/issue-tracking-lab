unit MatrixOutput;

interface
uses MatrixTreatment;

procedure Print(outputMatrix: TasksMatrix);
function GetMainDiagonalNegativCount(inputMatrix: TasksMatrix): Integer;

implementation
procedure Print(outputMatrix: TasksMatrix);
var i,j: Integer;
begin
    WriteLn;
    for i := 1 to RowCount do
    begin
        for j := 1 to ColumnCount do
        begin
            Write (outputMatrix[i, j]:4)
        end;
        WriteLn
    end;
    WriteLn
end;

function GetMainDiagonalNegativCount(inputMatrix: TasksMatrix): Integer;
var i, count: Integer;
begin
    count := 0;
    for i := 1 to RowCount do
    begin
        if inputMatrix[i,i]<0 then
        begin
            inc(count);
        end;
    end;
    result := count;
end;

end.
 