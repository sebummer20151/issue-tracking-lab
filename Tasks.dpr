program Tasks;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  MatrixTreatment,
  MatrixOutput;

var matrixA: TasksMatrix;

begin
    FillMatrixWithRandomValues(matrixA);
    Print(matrixA);
    WriteLn('Count negativ values in main diagonal: ',GetMainDiagonalNegativCount(matrixA));
    ZeroElementsAboveSecondaryDiagonal(matrixA);
    Print(matrixA);
    ReadLn;
end.
